
docker-image:
	@docker build -t dep_docker -f pkg/docker/Dockerfile .

start-container:
	@docker run \
		-td \
		-m 13g \
		--memory-swap 13g \
		--rm \
		--cpus 10 \
		--name dep_docker \
		-v `pwd`:/go/src/github.com/josecordaz/test_dep \
		-v $$HOME/.temp_cache/dep:/go/pkg/dep/ \
		-v $$HOME/.temp_cache/gocache:/root/.cache \
		dep_docker \
		sh

local:
	@rm -rf vendor
	@dep ensure
	@printf '\nSecond run\n'
	@time dep ensure -v

container: docker-image start-container
	@rm -rf vendor
	@echo 'First run'
	@docker exec -it dep_docker time dep ensure -v
	@printf '\nSecond run\n'
	@docker exec -it dep_docker time dep ensure -v
	-@docker rm -f dep_docker