package main

import (
	goplugi "github.com/josecordaz/test_dep/goplugin"
)

func main() {
	goplugi.GoPlugin()
}
